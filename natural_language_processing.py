import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# Importing the dataset
# putting the quoting we are ignoring the double quotes
dataset = pd.read_csv('Restaurant_Reviews.tsv', delimiter='\t', quoting=3)

import re
import nltk

# Cleaning the texts
nltk.download('stopwords')
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer

corpus = []

# 1000 is the number of lines in dataset
for i in range(0, 1000):

    # sub function keeps just what matches with the regex
    review = re.sub('[^a-zA-Z]', ' ', dataset['Review'][i])
    review = review.lower()
    review = review.split()

    # Class to take the base of the word
    ps = PorterStemmer()
    review = [ps.stem(word) for word in review if not word in set(stopwords.words('english'))]
    review = ' '.join(review)
    corpus.append(review)

# Creating the Bag of Words model
from sklearn.feature_extraction.text import CountVectorizer

# Cv will proceed with the tokenization to create the bag of words
cv = CountVectorizer(max_features = 1500)

# Bag of words itself
X = cv.fit_transform(corpus).toarray()

# Creating the depend variables vector, taking the colunm with the assesment
y = dataset.iloc[:,1].values

# Splitting the dataset into Training set and Test set
from sklearn.cross_validation import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20, random_state=0)


# # Feature Scaling (Normalization) ( Don't need because in this case there a lot of zeros,
# a little ones and twos)
# from sklearn.preprocessing import StandardScaler
# sc = StandardScaler()
# X_train = sc.fit_transform(X_train)
# X_test = sc.transform(X_test)

# Fitting Naive Bayes to the Training set
from sklearn.naive_bayes import GaussianNB
classifier = GaussianNB()
classifier.fit(X_train, y_train)

# Predicting the Test set results
y_pred = classifier.predict(X_test)

# Making the Confusion Matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, y_pred)

print(cm)
print((55+91)/200)